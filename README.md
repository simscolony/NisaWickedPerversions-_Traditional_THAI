
# คำเตือน! มีการใช้คำและภาษาหยาบคาย โปรดใช้สติในการเล่น! 
![LOGO_NisaWicked](https://user-images.githubusercontent.com/13219372/127160345-5d459706-60a2-4d54-af88-2f1d6d224750.jpg)
# แปลไทย Nisa’s Wicked 
## อัพเดท 13 กันยายน 2564 - อัตราการแปล 100%

| SIMSCOLONY THAI| NisaWicked 13-9-2021|รายละเอียด|
| ------------- | ------------- |------------- |
| Nisa’s Wicked V6| [ดาวโหลดแปล NisaWicked 6](https://github.com/simscolony/NisaWickedPerversions-_Traditional_THAI/raw/main/%5BSIMSCOLONY%5DNisaWickedPerversionsv_TH_V6_%5B13-9-2021%5D.package) |แปลไทยอย่างเดียวไม่มีตัวเล่น|
| Nisa’s Wicked V6 Patreon| [ดาวโหลดแปล NisaWicked 6 Patreon](https://www.patreon.com/posts/56103139) |แปลไทยและตัวยัดภาษาไทย|


*สามารถดาวโหลดไฟล์ใดก็ได้ เป็นไฟล์เดียวกันหมด
กรุณาเลือกติดตั้ง WickedWhims เพิ่อใช้งานร่วมกัน

# Update Last Support 
## Public Version Nisa’s Wicked Perversions LL.3.1ed
##  patreon Version NisaWickedPerversions  V2_2_4ha_content

### HOW TO Downlodws MOD PLAY 

# วิธีการลง
1. ติดตั้ง MOD ภาพไทย
https://simscolony.github.io/TS4THDEMO/

2. ดาวโหลด MOD หลัก WickedWhims  รุ่นฟรีดาวโหลดทาง
https://turbodriver.itch.io/wickedwhims
WickedWhims Mod made by TURBODRIVER   [Download wickedwhims](https://wickedwhimsmod.com/download/) OR
[loverslab](https://www.loverslab.com/files/file/5755-sims-4-thai-translation-for-wickedwhims-435140c-16-april-2019/)

3.ดาวโหลด MOD หลัก Nisa’s Wicked 

Patreon >https://www.patreon.com/NisaKsPervs 

Public https://www.loverslab.com/files/file/5002-nisa%E2%80%99s-wicked-perversions/?__cf_chl_jschl_tk__=pmd_19045f817a6b73b97b9a5946bea6129e996a0afe-1627392054-0-gqNtZGzNAg2jcnBszQiO

4.นำลงใส่ใน FLoder Mods
![บันทึกแบบเต็มหน้าจอ 27 ก ค  2564 202142 bmp](https://user-images.githubusercontent.com/13219372/127160921-8d2da4d9-af46-437a-97a5-816cc1d9ca05.jpg)



# หากพบปัญหา แจ้งงานแปล
## ติดต่อสอบถามได้ที่ [SIMSCOLONY](https://www.facebook.com/SimsColony/)

# ภายใต้การอนุญาติ 

Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-nc-sa/4.0/

https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

